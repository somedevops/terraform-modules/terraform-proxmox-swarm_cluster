# output "manager_nodes_params" {
#   for_each = {
#     for k, v in local.manager_nodes_array : "${v.name}" => v
#   }
#   name = each.value.name
#   memory = each.value.memory
# }
output "manager_names" {
  value = proxmox_vm_qemu.managers[*].name
}

# output "manager_ip" {
#   value = proxmox_vm_qemu.managers[0].default_ipv4_address
# }

output "manager_ips" {
  value = local.manager_node_ips
}
output "workers_names" {
  value = proxmox_vm_qemu.workers[*].name
}

# output "workers_ips" {
#   value = proxmox_vm_qemu.workers[*].default_ipv4_address
# }
output "worker_ips" {
  value = local.worker_node_ips
}