locals {
  manager_node_ips = [for i in range(var.manager_nodes_count) : cidrhost(var.swarm_subnet, i + 11)]
  worker_node_ips  = [for i in range(var.worker_nodes_count) : cidrhost(var.swarm_subnet, i + var.manager_nodes_count + 21)]
}