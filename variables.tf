variable "pm_api_url" {
}

variable "pm_api_token_id" {
}

variable "pm_api_token_secret" {
}

variable "proxmox_host" {
}

variable "ssh_key" {
}

variable "vm_template" {

}

variable "pvt_key" {

}

variable "manager_nodes_count" {
  description = "Number of swarm manager nodes"
  type        = number
  default     = 1
}

variable "worker_nodes_count" {
  description = "Number of swarm worker nodes"
  type        = number
  default     = 2
}

variable "swarm_subnet" {
  type = string
}

variable "manager_nodes" {
  description = "Swarm manager nodes parameters"
  type = object({
    cores          = optional(number),
    memory         = optional(number),
    disk_type      = optional(string),
    disk_size      = optional(string),
    name           = optional(string),
    storage        = optional(string),
    network_bridge = optional(string),
    gw             = optional(string)
  })

  default = {
    name      = "manager"
    cores     = 2
    memory    = 2048
    disk_type = "scsi"
  }
}

variable "worker_nodes" {
  description = "Swarm worker nodes parameters"
  type = object({
    name           = optional(string),
    storage        = optional(string),
    cores          = optional(number),
    memory         = optional(number),
    disk_type      = optional(string),
    disk_size      = optional(string),
    network_bridge = optional(string),
    gw             = optional(string)
  })

  default = {
    name      = "worker"
    cores     = 2
    memory    = 1512
    disk_type = "scsi"
  }

}