resource "proxmox_vm_qemu" "managers" {
  count       = var.manager_nodes_count
  name        = "${var.manager_nodes.name}${count.index + 1}"
  target_node = var.proxmox_host
  tags        = "manager"

  clone = var.vm_template

  onboot   = true
  agent    = 1
  os_type  = "cloud-init"
  cores    = var.manager_nodes.cores
  sockets  = 1
  cpu      = "host"
  memory   = var.manager_nodes.memory
  scsihw   = "virtio-scsi-pci"
  bootdisk = "scsi0"
  qemu_os  = "l26"

  disk {
    slot     = 0
    size     = var.manager_nodes.disk_size
    type     = var.manager_nodes.disk_type
    storage  = var.manager_nodes.storage
    iothread = 0
  }


  network {
    model  = "virtio"
    bridge = var.manager_nodes.network_bridge
  }


  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${local.manager_node_ips[count.index]}/24,gw=${var.manager_nodes.gw}"

  sshkeys = var.ssh_key

}

resource "proxmox_vm_qemu" "workers" {
  count       = var.worker_nodes_count
  name        = "${var.worker_nodes.name}${count.index + 1}"
  target_node = var.proxmox_host
  tags        = "worker"

  clone = var.vm_template

  onboot   = true
  agent    = 1
  os_type  = "cloud-init"
  cores    = var.worker_nodes.cores
  sockets  = 1
  cpu      = "host"
  memory   = var.worker_nodes.memory
  scsihw   = "virtio-scsi-pci"
  bootdisk = "scsi0"
  qemu_os  = "l26"

  disk {
    slot     = 0
    size     = var.worker_nodes.disk_size
    type     = var.worker_nodes.disk_type
    storage  = var.worker_nodes.storage
    iothread = 0
  }


  network {
    model  = "virtio"
    bridge = var.worker_nodes.network_bridge
  }


  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${local.worker_node_ips[count.index]}/24,gw=${var.worker_nodes.gw}"
  sshkeys   = var.ssh_key



}
